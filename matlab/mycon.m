function [c,ceq] = mycon(x)
c=[];

global L n X;
l=L/n;

ceq=[x(1)^2+x(2)^2-l^2];
for i=3:2:2*(n-1),
  ceq=[ceq; (x(i)-x(i-2))^2+(x(i+1)-x(i-1))^2-l^2];
end;
ceq=[ceq; (X-x(end-1))^2+x(end)^2-l^2];

