global L X n;
L=10; X=4;
n=40;

potencial=@(x) sum(x(2:2:end));

lb=zeros(2*(n-1),1);
lb(2:2:end)=-L;
ub=X*ones(2*(n-1),1);
ub(2:2:end)=0;

x0=zeros(2*(n-1),1);
x0(1:2:end-1)=X/n*(1:n-1);

options = optimset('Algorithm','sqp','MaxFunEvals',20000);
[xopt,fval]=fmincon(@(x) potencial(x),x0,[],[],[],[],lb,ub,@(x) mycon(x),options);

xopt=[0; 0; xopt; X; 0];

x=xopt(1:2:end-1);
y=xopt(2:2:end);

a=-min(y);
b=0.78; plot(x,y,'+r',x,b*cosh((x-X/2)/b)-b-a,'b');
z=0:0.1:4; plot(x,y,'*r',x,y,'c',z,b*cosh((z-X/2)/b)-b-a,'b');

